CREATE DATABASE company;
use company;
create table candidate(
firstName varchar(50),
middleName varchar(50),
lastName varchar(50),
vacancy varchar(60),
email varchar(60),
contact varchar(60),
keyword varchar(50),
notes varchar(50)
);

create table users(
id int auto_increment,
userRole varchar(50),
employeeName varchar(60),
status1 varchar(60),
userName varchar(50),
password1 varchar(50),
confirmPassword varchar(50),
PRIMARY KEY (id,userName)
);

create table login(
userName varchar(60),
password1 varchar(70)
);

insert into login (userName, password1) values('saurabh','12345');

delete from users where userName = null;

create table vacancy(
vacancyName varchar(70),
jobTitle varchar(50),
description1 varchar(80),
hiringManager varchar(60),
position1 varchar(50)
);

drop table vacancy; 
drop table candidate;
drop table login;
drop table users;

select * from candidate;
select*from users;
select*from login;
select * from vacancy;