package edit_user;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class EditUserController implements Initializable{
	@FXML
	private ComboBox userRole;

	@FXML
	private ComboBox status;

	@FXML
	private TextField employeeName;

	@FXML
	private TextField userName1;

	@FXML
	private TextField userName;

	@FXML
	private TextField cpassword;

	@FXML
	private TextField password;

	@FXML
	private Button admin;

	@FXML
	private Button recruitment;

	@FXML
	private Button dashboard;

	@FXML
	private Button cancel;

	@FXML
	private Button edit;

	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}

	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}

	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}

	public void edit(ActionEvent event) throws SQLException {
		   System.out.println(userName1.getText());
		   System.out.println( userRole.getValue());
		 	System.out.println(status.getValue());
			System.out.println(employeeName.getText());
			System.out.println(userName.getText());
			System.out.println(password.getText());
			System.out.println(cpassword.getText());
			
			if (userName1.getText().isEmpty()) {
	            Alert alert = new Alert(Alert.AlertType.ERROR);
	            alert.setTitle("Error");
	            alert.setHeaderText(null);
	            alert.setContentText("Please fill all the Fields.");
	            alert.showAndWait();
	            return;
			}
			String query1 ="SELECT count(*) FROM users WHERE userName ='"+userName1.getText()+"'; ";
	        ResultSet rs1 =DbUtil.executeQueryGetResult(query1);
	        rs1.next();
	        if(rs1.getInt(1)==1) {
	            	if (password.getText().equals(cpassword.getText())) {
	                    System.out.println("Passwords match");
			 		String query = "update users set employeeName ='" +employeeName.getText() + "',userRole ='"
					+userRole.getValue() + "',status"+ " ='" + status.getValue() + "',userName ='"
					+ userName.getText() + "',password1 ='" + password.getText() + "',confirmPassword ='" + cpassword.getText() + "' where userName='"
					+ userName1.getText()+"';";
		   System.out.println(query);
			DbUtil.executeQuery(query);
			System.out.println("Event occur Edit controller "+event.getEventType().getName());
			new Admin().show();
			 } }else {
				 Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("Fill correct Candidate");
					alert.showAndWait();
					return;
			 }
	 }
	  
	  @Override
		public void initialize(URL arg0, ResourceBundle arg1) {
		  ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
			userRole.setItems(list);
			//userRole.getItems().add(list);
			ObservableList<String> list2 = FXCollections.observableArrayList("Enabled", "Disabled");
			status.setItems(list2);
				
		}
	  public void ConPassword(ActionEvent event) {
			String Password = password.getText();
			String ConfirmPassword = cpassword.getText();
			if (Password.equals(ConfirmPassword)) {
				new Admin().show();
			} else {
				System.out.println("Data saved Unsuccesful");
			}
	  }
}
