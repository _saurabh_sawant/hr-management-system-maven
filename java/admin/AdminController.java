package admin;

import recruitment.Recruitment;
import search_user.SearchUser;
import add_user.AddUser;
import delete_user.DeleteUser;
import edit_user.EditUser;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;

public class AdminController {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;

	@FXML
	private Button addUser;

	@FXML
	private Button searchUser;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button editUser;

	@FXML
	private Button deleteUser;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;

	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}

	public void addUser(ActionEvent event) {
		new AddUser().show();
	}

	public void searchUser(ActionEvent event) {
		new SearchUser().show();
	}
	
	public void editUser(ActionEvent event) {
		new EditUser().show();
	}

	public void deleteUser(ActionEvent event) {
		new DeleteUser().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void exit(ActionEvent event) {
		new LoginScreen().show();
	}
}