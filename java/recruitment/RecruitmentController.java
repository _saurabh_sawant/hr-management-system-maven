package recruitment;

import admin.Admin;
import candidate.Candidate;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import vacancy.Vacancy;

public class RecruitmentController {
	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button search;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button candidate;
	
	@FXML
	private Button vacancy;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;
	

	public void admin(ActionEvent event) {
		new Admin().show();
	}
	
	public void search(ActionEvent event) {
		
	}
	
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	public void candidate(ActionEvent event) {
		new Candidate().show();
	}
	public void vacancy(ActionEvent event) {
		new Vacancy().show();
	}
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	public void exit(ActionEvent event) {
		new Admin().show();
	}
}
