package edit_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class EditCandidateController implements Initializable{

	@FXML
	private Button admin;

	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button edit;
	
	@FXML
	private ComboBox vacancy;
	
	@FXML
	private TextField firstName1;
	
	@FXML
	private TextField firstName;
	
	@FXML
	private TextField middleName;
	
	@FXML
	private TextField lastName;
	
	@FXML
	private TextField keyword;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField contact;
	
	@FXML
	private TextField notes;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void edit(ActionEvent event) throws SQLException {
		
		System.out.println(firstName1.getText());
		System.out.println(firstName.getText());
		System.out.println(middleName.getText());
		System.out.println(lastName.getText());
		System.out.println(vacancy.getValue());
		System.out.println(notes.getText());
		System.out.println(email.getText());
		System.out.println(keyword.getText());
		System.out.println(contact.getText());

		if (firstName1.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all the Fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "SELECT count(*) FROM candidate WHERE firstName ='" + firstName1.getText() + "'; ";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
				System.out.println("Passwords match");
				String query = "update candidate set firstName ='" + firstName.getText() + "',middleName ='"
						+ middleName.getText() + "',lastName" + " ='" + lastName.getText() + "',vacancy ='"
						+ vacancy.getValue() + "',notes ='" + notes.getText() + "',email ='" + email.getText()
						+ "' ,keyword" + " ='" + keyword.getText() + "' ,contact" + " ='" + contact.getText()
						+ "' where firstName='" + firstName1.getText() + "';";
				System.out.println(query);
				DbUtil.executeQuery(query);
				System.out.println("Event occur Edit controller " + event.getEventType().getName());
				new Admin().show();
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Fill correct Candidate");
			alert.showAndWait();
			return;
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ObservableList<String> list1 = FXCollections.observableArrayList("Associate IT Manager", "Sales Representative");
		vacancy.setItems(list1);

	}

	

}
