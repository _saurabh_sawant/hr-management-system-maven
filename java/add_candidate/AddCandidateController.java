package add_candidate;

import java.net.URL;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class AddCandidateController implements Initializable {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button add;
	
	@FXML
	private ComboBox vacancy;
	
	@FXML
	private TextField firstName;
	
	@FXML
	private TextField middleName;
	
	@FXML
	private TextField lastName;
	
	@FXML
	private TextField keyword;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField contact;
	
	@FXML
	private TextField notes;
	
	
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void add(ActionEvent event){
		
		if (firstName.getText().isEmpty() || middleName.getText().isEmpty() || lastName.getText().isEmpty()
				|| notes.getText().isEmpty() || email.getText().isEmpty() || keyword.getText().isEmpty()
				|| contact.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		
		System.out.println(firstName.getText());
		System.out.println(middleName.getText());
		System.out.println(lastName.getText());
		System.out.println(vacancy.getValue());
		System.out.println(notes.getText());
		System.out.println(email.getText());
		System.out.println(keyword.getText());
		System.out.println(contact.getText());

		String query = "insert into candidate(firstName,middleName,lastName,vacancy,keyword,email,contact,notes) values ('"
				+ firstName.getText() + "', '" + middleName.getText() + "','" + lastName.getText() + "','"
				+ vacancy.getValue() + "','" + keyword.getText() + "','" + email.getText()  + "','"
				+ contact.getText() + "','" + notes.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());
		
		new Recruitment().show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list1 = FXCollections.observableArrayList("Associate IT Manager", "Sales Representative");
		vacancy.setItems(list1);
		
		
	}
}
