package add_vacancy;

import java.net.URL;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class AddVacancyController implements Initializable{
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button add;
	
	@FXML
	private TextField vacancyName;
	
	@FXML
	private ComboBox jobTitle;
	
	@FXML
	private TextField description1;
	
	@FXML
	private TextField hiringManager;
	
	@FXML
	private TextField position1;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void add(ActionEvent event) {
		
		if (vacancyName.getText().isEmpty() || description1.getText().isEmpty() || hiringManager.getText().isEmpty()
				|| position1.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		
		System.out.println(vacancyName.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(description1.getText());
		System.out.println(hiringManager.getText());
		System.out.println(position1.getText());
		
		String query = "insert into vacancy(vacancyName,jobTitle,description1,hiringManager,position1) values ('"
				+ vacancyName.getText() + "', '" + jobTitle.getValue() + "','" + description1.getText() + "','"
				+ hiringManager.getText() + "','" + position1.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());
		
		new Recruitment().show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list = FXCollections.observableArrayList("Account Assistant", "Content Specialist");
		jobTitle.setItems(list);
	}
}
