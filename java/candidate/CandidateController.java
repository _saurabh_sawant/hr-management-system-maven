package candidate;

import add_candidate.AddCandidate;
import admin.Admin;
import delete_candidate.DeleteCandidate;
import edit_candidate.EditCandidate;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;
import recruitment.Recruitment;
import search_candidate.SearchCandidate;

public class CandidateController {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button addCandidate;
	
	@FXML
	private Button editCandidate;
	
	@FXML
	private Button searchCandidate;
	
	@FXML
	private Button deleteCandidate;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void exit(ActionEvent event) {
		new LoginScreen().show();
	}
	
	public void addCandidate(ActionEvent event) {
		new AddCandidate().show();
	}
	
	public void editCandidate(ActionEvent event) {
		new EditCandidate().show();
	}

	public void searchCandidate(ActionEvent event) {
		new SearchCandidate().show();
	}

	public void deleteCandidate(ActionEvent event) {
		new DeleteCandidate().show();
	}

}
