package add_user;

import java.net.URL;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import login_screen.LoginScreen;
import login_screen.LoginScreenICP;
import recruitment.Recruitment;

public class AddUserController implements Initializable {
	@FXML
	private ComboBox userRole;
	
	@FXML
	private ComboBox status;
	
	@FXML
	private TextField employeeName;
	
	@FXML
	private TextField userName;

	@FXML
	private TextField cpassword;

	@FXML
	private TextField password;
	
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button add;
	
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void add(ActionEvent event) {
		System.out.println(userRole.getValue());
		System.out.println(status.getValue());
		System.out.println(employeeName.getText());
		System.out.println(userName.getText());
		System.out.println(password.getText());
		System.out.println(cpassword.getText());
		
		
		
		if (cpassword.getText().equals(password.getText())) {
			
			if (cpassword.getText().isEmpty() || employeeName.getText().isEmpty() || userName.getText().isEmpty()
					|| password.getText().isEmpty()) {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Please fill in all fields.");
				alert.showAndWait();
				return;
			}

			String query = "insert into users(userRole,status,employeeName,userName,password1,confirmPassword) values ('"
					+ userRole.getValue() + "', '" + status.getValue() + "','" + employeeName.getText() + "','"
					+ userName.getText() + "','" + password.getText() + "','" + cpassword.getText() + "');";
			DbUtil.executeQuery(query);
			System.out.println(query);
			
			
			
			System.out.println("Event occur admin controller " + event.getEventType().getName());
			System.out.println("Login Succesfully");
			
			new Admin().show();
		}
		
		else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Passsword and Confirm Password should be same.");
			alert.showAndWait();
			return;
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
		userRole.setItems(list);
		//userRole.getItems().add(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Enabled", "Disabled");
		status.setItems(list2);
	}
}


