package login_screen;

import java.sql.ResultSet;
import java.sql.SQLException;

import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class LoginScreenController {
	@FXML
	private TextField userName;

	@FXML
	private TextField password1;

	@FXML
	private Button login;

	public void login(ActionEvent event) throws SQLException {

		if (userName.getText().isEmpty() || password1.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}

		/*String query = "Select * from users ";
		System.out.println(query);
		ResultSet resultSet = DbUtil.executeQueryGetResult(query);
		while (resultSet.next()) {
			if (userName.getText().contentEquals(resultSet.getString(5))
					&& password1.getText().contentEquals(resultSet.getString(6))) {
				System.out.println("Login Succesfully");
				new HomeScreen().show();
				break;

			} else {
				System.out.println("Login Failed");
				new LoginScreenICP().show();
			}
		}*/
		
		String query = "Select count(*) from users where userName='" + userName.getText() + "' and password1='"
				+ password1.getText() + "';";
		System.out.println(query);
		ResultSet resultSet = DbUtil.executeQueryGetResult(query);
		resultSet.next(); // cursor will come to next value
		if (resultSet.getInt(1) == 1) {
			new HomeScreen().show();
		} else {
			System.out.println("Login Failed");
			new LoginScreenICP().show();
		}
	}
}