package search_candidate;

import javafx.beans.property.SimpleStringProperty;

public class UserCandidate {
	public SimpleStringProperty firstName=new SimpleStringProperty();
	public SimpleStringProperty middleName=new SimpleStringProperty();
	public SimpleStringProperty lastName=new SimpleStringProperty();
	public SimpleStringProperty vacancy=new SimpleStringProperty();
	public SimpleStringProperty keyword=new SimpleStringProperty();
	public SimpleStringProperty email=new SimpleStringProperty();
	public SimpleStringProperty contact=new SimpleStringProperty();
	public SimpleStringProperty notes=new SimpleStringProperty();
	
	public String getFirstName(){
	       return firstName.get();
	   }
	
	public String getMiddleName(){
	       return middleName.get();
	   }
	
	public String getLastName(){
	       return lastName.get();
	   }
	
	public String getVacancy(){
	       return vacancy.get();
	   }
	
	public String getKeyword(){
	       return keyword.get();
	   }
	
	public String getEmail(){
	       return email.get();
	   }
	
	public String getContact(){
	       return contact.get();
	   }
	
	public String getNotes(){
	       return notes.get();
	   }

}
