package homescreen;


import admin.Admin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;
import recruitment.Recruitment;


public class HomeScreenController {
	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button exit;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}
	
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void exit(ActionEvent event) {
		new LoginScreen().show();
	}	
}
