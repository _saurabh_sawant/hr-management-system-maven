package edit_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class EditVacancyController implements Initializable{
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;

	@FXML
	private Button dashboard;

	@FXML
	private Button cancel;

	@FXML
	private Button edit;

	@FXML
	private TextField vacancyName1;

	@FXML
	private TextField vacancyName;

	@FXML
	private ComboBox jobTitle;

	@FXML
	private TextField description1;

	@FXML
	private TextField hiringManager;

	@FXML
	private TextField position1;

	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}

	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}

	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}

	public void edit(ActionEvent event) throws SQLException {

		System.out.println(vacancyName1.getText());
		System.out.println(vacancyName.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(description1.getText());
		System.out.println(hiringManager.getText());
		System.out.println(position1.getText());
	

		if (vacancyName1.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all the Fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "SELECT count(*) FROM vacancy WHERE vacancyName ='" + vacancyName1.getText() + "'; ";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
			System.out.println("Passwords match");
			String query = "update vacancy set vacancyName ='" + vacancyName.getText() + "',jobTitle ='"
					+ jobTitle.getValue() + "',description1" + " ='" + description1.getText() + "',hiringManager ='"
					+ hiringManager.getText() + "',position1 ='" + position1.getText() + "' where vacancyName='" + vacancyName1.getText() + "';";
			System.out.println(query);
			DbUtil.executeQuery(query);
			System.out.println("Event occur Edit controller " + event.getEventType().getName());
			new Admin().show();
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Fill correct Candidate");
			alert.showAndWait();
			return;
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ObservableList<String> list = FXCollections.observableArrayList("Account Assistant", "Content Specialist");
		jobTitle.setItems(list);
	}

}
